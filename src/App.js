import React, {Component} from 'react';
import CategoryForm from "./component/AddCategory";
import AddedCategory from "./component/AddedCategory";

import './App.css';


class App extends Component {
    state = {
        value: "Add new movie",
        categories: [],

    };

    changeCategory = (event) => {
        this.setState({value: event.target.value});
    };

    changeCategoryText = (event, id) => {
        const categories = [...this.state.categories];
        let category = {...categories[id]};
        category.text = event.target.value;
        categories[id] = category;

        this.setState({categories});
    };
    changeValueForEmpty = () => {
        this.setState({value: ''})
    };

    addCategory = (event) => {
        if (this.state.value !== " " && this.state.value !== "Add new movie") {
            const categoryArray = [...this.state.categories];
            event.preventDefault();

            let newCategory = {};
            newCategory.text = this.state.value;
            categoryArray.push(newCategory);

            this.setState({value: ' ', categories: categoryArray})
        } else {
            event.preventDefault();
            alert("Please, enter a movie title")
        }
    };

    removeCategory = (id) => {
        const allCategories = [...this.state.categories];
        allCategories.splice(id, 1);
        this.setState({categories: allCategories});
    };

    render() {
        let categories =
            <AddedCategory
                categories={this.state.categories}
                remove={this.removeCategory}
                changeCategoryText={this.changeCategoryText}
            >
            </AddedCategory>;

        return (
            <div className="App">
                <CategoryForm value={this.state.value}
                              changeValue={event => this.changeCategory(event)}
                              onClick={event => this.addCategory(event)}
                              onFocus={this.changeValueForEmpty}
                >
                </CategoryForm>
                {categories}
            </div>
        );
    }
}

export default App;
