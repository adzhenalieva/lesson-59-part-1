import React, {Component} from 'react';

class AddedInput extends Component {


    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.text !== this.props.text
    };

    render() {
        return (
            <div className="categoryDiv">
                <input className="AddedCategory" type="text" value={this.props.text} onChange={this.props.changeText}/>
                <button className="btnDelete" onClick={this.props.remove}>x</button>
            </div>
        );
    }
}

export default AddedInput;