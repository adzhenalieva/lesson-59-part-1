import React, {Component} from "react";
import AddedInput from "./AddedInput";

class AddedCategory extends Component {
    render() {

        return this.props.categories.map((category, id) => {
            return <AddedInput key={id} text={category.text}
                               changeText={(event) => this.props.changeCategoryText(event, id)}
                               remove={() => this.props.remove(id)}
            />
        })
    }
}

export default AddedCategory;
