import React from "react";

const CategoryForm = props => (
    <form>
        <p>
            <input className="categoryInput" type="text" value={props.value} onFocus={props.onFocus} onChange={props.changeValue}/>
        </p>
        <button className="btnAdd" onClick={props.onClick}>Add</button>
    </form>

);
export default CategoryForm;